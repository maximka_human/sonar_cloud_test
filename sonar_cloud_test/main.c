#include <stdio.h>

void SystemInit()
{
}

static int a[5] = { 0xff, 0xff, 0xff, 0xff, 0xff };
#define  count_of(a) (sizeof(a)/sizeof((a)[0]))

typedef union {
    uint8_t a[50];
} U;

void foo(U u)
{
    for (uint8_t i = 0; i < count_of(u.a); ++i) {
        printf("%u\n", u.a[i]);
    }
}

typedef struct {
    uint8_t a[50];
} S;

void bar(S s)
{
    for (uint8_t i = 0; i < count_of(s.a); ++i) {
        printf("%u\n", s.a[i]);
    }
}

int main()
{
    U u = {};
    foo(u);

    S s = {};
    bar(s);

    for (int i = count_of(a); i > 0;) {
        a[--i] = 0;
    }

    printf("%d", a[0]);

    static const char kMessage[] = "Hello world";
    printf("%s", kMessage);

    return 0;
}